const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WasmPackPlugin = require('@wasm-tool/wasm-pack-plugin');

const dist = path.resolve(__dirname, 'dist');

const config = {
	mode: 'production',
	entry: {
		bootstrap: [
			'./pkg/index.js',
			'./src/app.css',
		],
	},
	output: {
		path: dist,
		filename: '[contenthash].js'
	},
	devServer: {
		contentBase: dist,
	},
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'postcss-loader',
				],
			},
			{
				test: /\.svg$/,
				loader: 'svg-url-loader',
			}
		],
	},
	plugins: [
		new WasmPackPlugin({
			crateDirectory: __dirname,
		}),

		new MiniCssExtractPlugin({
			filename: '[contenthash].css'
		}),

		new HtmlWebpackPlugin({
			title: 'tabata-wasm'
		}),

	],
	experiments: {
		asyncWebAssembly: true
	},
};

module.exports = (env, args) => {
	const IS_DEV = args.mode === 'development';

	if (IS_DEV) {
		return config;
	}

	const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
	const CompressionPlugin = require('compression-webpack-plugin');
	const zopfli = require('@gfx/zopfli');

	return {
		...config,
		plugins: [
			...config.plugins,

			new CompressionPlugin({
				test: /\.(js|css|wasm)$/,
				compressionOptions: {
					numiterations: 15,
				},
				algorithm(input, compressionOptions, callback) {
					return zopfli.gzip(input, compressionOptions, callback);
				},
			}),
		],
		optimization: {
			minimizer: [
				`...`,
				new CssMinimizerPlugin(),
			],
		},
	}
}
