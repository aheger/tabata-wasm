use super::time_slot::{TimeSlot, TimeSlotKind};

#[derive(Clone, Debug, PartialEq)]
pub enum TimeSlotTransitionKind {
    Start(TimeSlotKind),
    End(usize),
}

pub fn get_time_slot_transition(slot: &TimeSlot, time_done: usize) -> Option<TimeSlotTransitionKind> {
    if time_done == 0 {
        return Some(TimeSlotTransitionKind::Start(slot.kind))
    }

    let time_left = slot.duration - time_done;
    if time_left <= 3 {
        return Some(TimeSlotTransitionKind::End(time_left))
    }

    None
}
