mod app;
pub mod session;

use wasm_bindgen::prelude::*;
use yew::services::ConsoleService;

// This is like the `main` function, except for JavaScript.
#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    ConsoleService::debug("[APP] Start");

    yew::start_app::<app::App>();

    Ok(())
}
