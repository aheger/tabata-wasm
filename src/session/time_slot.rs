#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TimeSlotKind {
    Preparation,
    Rest,
    Work,
}

#[derive(Clone, Debug, PartialEq)]
pub struct TimeSlot {
    pub kind: TimeSlotKind,
    pub duration: usize,
}

fn duration_from_kind(kind: &TimeSlotKind) -> usize {
    match kind {
        TimeSlotKind::Preparation => 20,
        TimeSlotKind::Rest => 10,
        TimeSlotKind::Work => 20,
    }
}

impl TimeSlot {
    pub fn new(kind: TimeSlotKind) -> TimeSlot {
        let duration = duration_from_kind(&kind);

        TimeSlot { kind, duration }
    }
}
