let _context;

/**
 * init audo context
 */
function initContext() {
	if (!_context) {
		_context = new (window.AudioContext || window.webkitAudioContext)();
	}

	return _context.resume();
}

/**
 * play "beep" sound
 *
 * @param {float} frequency frequency of the beep in Hz
 * @param {float} duration duration of the beep in seconds
 * @param {float} volume volume of beep from 0 to 1
 */
function beepFunc(frequency, duration, volume) {
	frequency = frequency || 1000;
	duration = duration || 0.5;
	volume = volume || 1;

	// instantiate an oscillator
	const osc = _context.createOscillator();
	osc.type = 'triangle';
	osc.frequency.setValueAtTime(frequency, _context.currentTime);

	// create gain node
	const vol = _context.createGain();
	vol.gain.setValueAtTime(volume, _context.currentTime);

	// connect gain node to oscillator
	osc.connect(vol);
	// connect gain node to destination
	vol.connect(_context.destination);

	// start the oscillator
	osc.start();
	osc.stop(_context.currentTime + duration);
}

export function callBeep(frequency) {
	initContext().then(() => beepFunc(frequency, 0.4, 0.1));
}
