use wasm_bindgen::prelude::*;

// wasm-bindgen will automatically take care of including this script
#[wasm_bindgen(module = "/src/app/beep/beep.js")]
extern "C" {
    #[wasm_bindgen(js_name = "callBeep")]
    pub fn beep(frequency: JsValue);
}

