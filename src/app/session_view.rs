use wasm_bindgen::prelude::*;
use yew::prelude::*;
use yew::Callback;

use super::beep::beep;
use super::timeslot_view::TimeSlotView;
use crate::session::Session;
use crate::session::time_slot_transition::{get_time_slot_transition, TimeSlotTransitionKind};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub timer: usize,
    pub session: Session,
    pub on_done: Callback<()>,
}

pub struct SessionView {
    props: Props,
    current_time_slot: (usize, usize),
}

impl Component for SessionView {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self {
            props,
            current_time_slot: (0, 0),
        }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;

        let new_time_slot = get_current_time_slot(&self.props.session, &self.props.timer);

        if new_time_slot.0 < self.props.session.time_slots.len() {
            self.current_time_slot = new_time_slot;

            if self.props.timer > 0 {
                let transition = get_time_slot_transition(
                    &self.props.session.time_slots[self.current_time_slot.0],
                    self.current_time_slot.1,
                );

                match transition {
                    Some(TimeSlotTransitionKind::End(_)) => beep(JsValue::from_f64(2000.)),
                    Some(TimeSlotTransitionKind::Start(_)) => beep(JsValue::from_f64(3000.)),
                    _ => {}
                }
            }
        } else {
            self.props.on_done.emit(());
            self.current_time_slot = (0, 0);
        }

        true
    }

    fn view(&self) -> Html {
        let time_slot = self.props.session.time_slots.get(self.current_time_slot.0).unwrap();

        html! {
            <session-view>
            <TimeSlotView time_slot={time_slot} is_current={true} time_done={self.current_time_slot.1} />
            </session-view>
        }
    }
}

fn get_current_time_slot(session: &Session, timer: &usize) -> (usize, usize) {
    let mut current_time_slot = 0;
    let mut time_to_spread = *timer;

    for time_slot in session.time_slots.iter() {
        if time_to_spread < time_slot.duration {
            break;
        }
        time_to_spread -= time_slot.duration;

        current_time_slot += 1;
    }

    (current_time_slot, time_to_spread)
}
