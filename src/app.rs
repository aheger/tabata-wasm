mod session_view;
mod timeslot_view;
mod beep;

use yew::prelude::*;

use std::time::Duration;
use yew::services::{IntervalService, Task};

use session_view::SessionView;

use crate::session::Session;

pub struct App {
    cb_tick: Callback<()>,
    interval_job: Option<Box<dyn Task>>,
    link: ComponentLink<Self>,
    session: Session,
    timer: usize,
}

pub enum Msg {
    StartSession,
    Tick,
    EndSession,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        App {
            cb_tick: link.callback(|_| Msg::Tick),
            interval_job: None,
            session: Session::default(),
            timer: 0,
            link,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::StartSession => {
                let handle = IntervalService::spawn(Duration::from_secs(1), self.cb_tick.clone());
                self.interval_job = Some(Box::new(handle));
            }
            Msg::EndSession => {
                self.interval_job.take();
                self.timer = 0;
            }
            Msg::Tick => {
                self.timer += 1;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let render_button = move || -> Html {
            if self.interval_job.is_some() {
                html! {
                    <button class="session-trigger primary icon-button" onclick=self.link.callback(|_| Msg::EndSession)>
                        <span class="icon stop" /> { " End Session" }
                    </button>
                }
            } else {
                html! {
                    <button class="session-trigger primary icon-button" onclick=self.link.callback(|_| Msg::StartSession)>
                        <span class="icon play" />{ "Start Session" }
                    </button>
                }
            }
        };

        html! {
            <div>
                { render_button() }
                <SessionView session={&self.session} timer=&self.timer on_done=&self.link.callback(|_| Msg::EndSession)/>
            </div>
        }
    }
}
