# tabata-wasm

[Tabata](https://en.wikipedia.org/wiki/High-intensity_interval_training) Timer using WebAssembly  
  
[Try it out](https://aheger.gitlab.io/tabata-wasm/)  

## How to install

```sh
npm install
```

## How to run in debug mode

```sh
# Builds the project and opens it in a new browser tab. Auto-reloads when the project changes.
npm start
```

## How to build in release mode

```sh
# Builds the project and places it into the `dist` folder.
npm run build
```
