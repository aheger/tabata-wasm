use yew::prelude::*;

use crate::session::time_slot::{TimeSlot, TimeSlotKind};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub time_slot: TimeSlot,
    pub is_current: bool,
    pub time_done: usize,
}

pub struct TimeSlotView {
    props: Props
}

impl Component for TimeSlotView {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        TimeSlotView {
            props
        }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        let mut classes = Vec::<String>::new();

        let props = &self.props;

        if props.is_current {
            classes.push("current".to_owned());

            let quantized_percentage = (((props.time_done as f32 * 100. / props.time_slot.duration as f32) / 5.).round() * 5.) as u32;
            classes.push(format!("done-{}", quantized_percentage));
        }


        let descr = match props.time_slot.kind {
            TimeSlotKind::Work => "Work",
            TimeSlotKind::Rest => "Rest",
            TimeSlotKind::Preparation => "Preparation",
        };

        html! {
            <time-slot-view class={classes.join(" ")}>
                <span class="description">{ descr }</span>
                <span class="time-left">{ props.time_slot.duration - props.time_done }</span>
            </time-slot-view>
        }
    }
}
