pub mod time_slot;
pub mod time_slot_transition;

use std::vec::Vec;

use time_slot::{TimeSlot, TimeSlotKind};

#[derive(Clone, PartialEq)]
pub struct Session {
    pub time_slots: Vec<TimeSlot>,
}

impl Default for Session {
    fn default() -> Self {
        let time_slots = vec![
            TimeSlotKind::Preparation,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
            TimeSlotKind::Rest,
            TimeSlotKind::Work,
        ]
        .into_iter()
        .map(|kind| TimeSlot::new(kind))
        .collect();

        Session { time_slots }
    }
}
